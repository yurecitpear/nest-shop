import {
	Body,
	Controller,
	Get,
	HttpCode,
	Post,
	UsePipes,
	ValidationPipe,
	UploadedFile,
	UseInterceptors
} from '@nestjs/common'
import { Auth } from 'src/auth/decorators/auth.decorator'
import { CurrentUser } from 'src/auth/decorators/user.decorator'
import { OrderDto, OrderDtoProvider } from './order.dto'
import { OrderService } from './order.service'
import { PaymentStatusDto } from './payment-status.dto'
import { diskStorage } from 'multer'
import { FileInterceptor } from '@nestjs/platform-express'
import { extname } from 'path'

@Controller('orders')
export class OrderController {
	constructor(private readonly orderService: OrderService) {}

	@Get()
	@Auth('admin')
	getAll() {
		return this.orderService.getAll()
	}

	@Get('by-user')
	@Auth()
	getByUserId(@CurrentUser('id') userId: number) {
		return this.orderService.getByUserId(userId)
	}

		// placeOrder(@Body() dto: OrderDto, @CurrentUser('id') userId: number) {
	@UsePipes(new ValidationPipe())
	@HttpCode(200)
	@Post()
	placeOrder(@Body() dto: OrderDto) {
		return this.orderService.placeOrder(dto)
	}

	@UsePipes(new ValidationPipe())
	@HttpCode(200)
	@Post('provider')
	@UseInterceptors(FileInterceptor('file', {
		storage: diskStorage({
			destination: './uploads',
			filename: (req, file, callback) => {
				if (!file) {
					return callback(new Error('No file provided'), null);
				}
				const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1e9);
				const ext = extname(file.originalname);
				const filename = `${file.originalname}-${uniqueSuffix}${ext}`;
				callback(null, filename);
			},
		}),
	}))
	placeOrderProvider(@Body() dto: OrderDtoProvider, @UploadedFile() file: Express.Multer.File) {
		if (file) {
			return this.orderService.placeOrderProvider(dto, file)
		}
		return this.orderService.placeOrderProvider(dto, null)
	}

	@HttpCode(200)
	@Post('status')
	async updateStatus(@Body() dto: PaymentStatusDto) {
		return this.orderService.updateStatus(dto)
	}
}
