import { EnumOrderStatus } from '@prisma/client'
import { Type } from 'class-transformer'
import {
	IsArray,
	IsEnum,
	IsNumber,
	IsString,
	IsOptional,
	ValidateNested,
	IsEmail,
	IsUrl,
	
} from 'class-validator'

export class OrderDto {
	@IsOptional()
	@IsEnum(EnumOrderStatus)
	status: EnumOrderStatus

	@IsArray()
	@ValidateNested({ each: true })
	@Type(() => OrderItemDto)
	items: OrderItemDto[]

	@IsString()
	name: string

	@IsString()
	phone: string

	@IsString()
	addres: string

	@IsString()
	email: string
}

export class OrderItemDto {
	@IsNumber()
	quantity: number

	@IsNumber()
	price: number

	@IsNumber()
	productId: number

	@IsString()
	name: string

	@IsString()
	image: string
}

export class OrderDtoProvider {
  @IsString()
  @IsOptional()
  category: string | null;

  @IsString()
  @IsOptional()
  nameOrganization: string | null;

  @IsString()
  @IsOptional()
  region: string | null;

  @IsString()
  @IsOptional()
  inn: string | null;

  @IsString()
  @IsOptional()
  contactFullName: string | null;

  @IsString()
  @IsOptional()
  position: string | null;

  @IsString()
  @IsOptional()
  contactPhone: string | null;

  @IsEmail()
  @IsOptional()
  email: string | null;

  @IsUrl()
  @IsOptional()
  companyWebsite: string | null;

  @IsString()
  @IsOptional()
  delivery: string | null;

  @IsString()
  @IsOptional()
  typeOfActivity: string | null;

  // Note: NestJS doesn't have built-in File validation decorator.
  file: File | null;

  @IsString()
  @IsOptional()
  additionalInfo: string | null;
}
