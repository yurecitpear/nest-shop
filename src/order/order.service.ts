import { Injectable } from '@nestjs/common'
import { EnumOrderStatus } from '@prisma/client'
import { PrismaService } from 'src/prisma.service'
import { productReturnObject } from 'src/product/return-product.object'
import * as YooKassa from 'yookassa'
import { OrderDto, OrderDtoProvider } from './order.dto'
import { PaymentStatusDto } from './payment-status.dto'

import * as nodemailer from 'nodemailer'
import * as TelegramBot from 'node-telegram-bot-api'

const yooKassa = new YooKassa({
	shopId: process.env['SHOP_ID'],
	secretKey: process.env['PAYMENT_TOKEN']
})

@Injectable()
export class OrderService {
	constructor(private prisma: PrismaService) {}

	async getAll() {
		return this.prisma.order.findMany({
			orderBy: {
				createdAt: 'desc'
			},
			include: {
				items: {
					include: {
						product: {
							select: productReturnObject
						}
					}
				}
			}
		})
	}

	async getByUserId(userId: number) {
		return this.prisma.order.findMany({
			where: {
				// userId
			},
			orderBy: {
				createdAt: 'desc'
			},
			include: {
				items: {
					include: {
						product: {
							select: productReturnObject
						}
					}
				}
			}
		})
	}

	// async placeOrder(dto: OrderDto, userId: number) {
	
	async placeOrder(dto: OrderDto) {
		const total = dto.items.reduce((acc, item) => {
			return acc + item.price * item.quantity
		}, 0)

		const order = await this.prisma.order.create({
			data: {
				status: dto.status,
				name: dto.name,
				phone: dto.phone,
				addres: dto.addres,
				email: dto.email,
				items: {
					create: dto.items
				},
				total,
			}
		})

		// Отправить уведомление на почту
    this.sendEmailNotification(order, dto.items)

    // Отправить уведомление в Telegram
    // this.sendTelegramNotification(order)

		// const payment = await yooKassa.createPayment({
		// 	amount: {
		// 		value: total.toFixed(2),
		// 		currency: 'RUB'
		// 	},
		// 	payment_method_data: {
		// 		/* CHANGE */
		// 		type: 'bank_card'
		// 	},
		// 	confirmation: {
		// 		type: 'redirect',
		// 		/* CHANGE */
		// 		return_url: 'http://localhost:3000/thanks'
		// 	},
		// 	/* CHANGE */
		// 	description: `Order #${order.id}`
		// })

		return order
	}

	private sendEmailNotification(order, items) {
		
		const transporter = nodemailer.createTransport({
      host: 'smtp.yandex.ru',
			port: 465,
			secure: true,
      auth: {
        user: 'info@bleskhome.ru',
        pass: 'bmceqzeixzhjkxig',
      },
			tls:{
				rejectUnAuthorized:true
			}
    })

    const itemsHtml = Array.isArray(items)
		? items.map(item => `
			<p>Товар:</p>
			<ul>
				<li>Наименование: ${item.name}</li>
				<li>Картинка: https://back.bleskhome.ru${item.image}</li>
				<li>Цена: ${item.price}</li>
				<li>ID продукта: ${item.productId}</li>
				<li>Количество: ${item.quantity}</li>
			</ul>
		`).join('')
		: '';

		const mailOptions = {
			from: 'info@bleskhome.ru',
			to: 'info@bleskhome.ru',
			subject: 'Ваш заказ успешно создан',
			html: `
				<p>Статус: Новый заказ</p>
				<p>Имя: ${String(order.name)}</p>
				<p>Телефон: ${String(order.phone)}</p>
				<p>Адрес: ${String(order.addres)}</p>
				<p>Email: ${String(order.email)}</p>
				<p>Товары:</p>
				${itemsHtml}
				<p>Итого: ${String(order.total)}</p>
			`
		}

    transporter.sendMail(mailOptions, (error, info) => {
      if (error) {
        console.log('Ошибка при отправке письма: ', error);
      } else {
        console.log('Письмо успешно отправлено: ', info.response);
      }
    })
  }

	async placeOrderProvider(dto: OrderDtoProvider, file: any) {

		const transporter = nodemailer.createTransport({
      host: 'smtp.yandex.ru',
			port: 465,
			secure: true,
      auth: {
        user: 'info@bleskhome.ru',
        pass: 'bmceqzeixzhjkxig',
      },
			tls:{
				rejectUnAuthorized:true
			}
    })

		const html = `
			<p>Категория: ${dto.category ?? 'Не указано'}</p>
			<p>Наименование организации: ${dto.nameOrganization ?? 'Не указано'}</p>
			<p>Адрес: ${dto.region ?? 'Не указано'}</p>
			<p>ИНН: ${dto.inn ?? 'Не указано'}</p>
			<p>Контактное лицо: ${dto.contactFullName ?? 'Не указано'}</p>
			<p>Должность: ${dto.position ?? 'Не указано'}</p>
			<p>Телефон для связи: ${dto.contactPhone ?? 'Не указано'}</p>
			<p>Электронная почта: ${dto.email ?? 'Не указано'}</p>
			<p>Сайт компании: ${dto.companyWebsite ?? 'Не указано'}</p>
			<p>Доставка: ${dto.delivery ?? 'Не указано'}</p>
			<p>Вид деятельности: ${dto.typeOfActivity ?? 'Не указано'}</p>
			<p>Дополнительная информация: ${dto.additionalInfo ?? 'Не указано'}</p>
		`;

		const mailOptions = {
			from: 'info@bleskhome.ru',
			to: 'info@bleskhome.ru',
			subject: 'Заявка на партнерство',
			html: html,
			attachments: [
        {
					filename: file.originalname,
					path: file.path
        }
			]
		}

    transporter.sendMail(mailOptions, (error, info) => {
      if (error) {
        console.log('Ошибка при отправке письма: ', error);
      } else {
        console.log('Письмо успешно отправлено: ', info.response);
      }
    })
	}


  private sendTelegramNotification(order) {
    const bot = new TelegramBot('YOUR_TELEGRAM_BOT_TOKEN', { polling: true });
    const adminChatId = 'YOUR_ADMIN_TELEGRAM_CHAT_ID';

    const message = `Новый заказ создан:\n\n`
      + `Имя: ${order.name}\n`
      + `Телефон: ${order.phone}\n`
      + `Адрес: ${order.address}\n`
      + `Email: ${order.email}`;

    bot.sendMessage(adminChatId, message)
      .then(() => {
        console.log('Сообщение успешно отправлено в Telegram');
      })
      .catch((error) => {
        console.log('Ошибка при отправке сообщения в Telegram: ', error);
      });
  }

	async updateStatus(dto: PaymentStatusDto) {
		if (dto.event === 'payment.waiting_for_capture') {
			const payment = await yooKassa.capturePayment(dto.object.id)
			return payment
		}

		if (dto.event === 'payment.succeeded') {
			const orderId = Number(dto.object.description.split('#')[1])

			await this.prisma.order.update({
				where: {
					id: orderId
				},
				data: {
					status: EnumOrderStatus.PAYED
				}
			})

			return true
		}

		return true
	}
}
