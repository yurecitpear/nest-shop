import { Module } from '@nestjs/common'
import { PrismaService } from 'src/prisma.service'
import { CategoryController } from './category.controller'
import { CategoryService } from './category.service'
import { MulterModule } from '@nestjs/platform-express'

@Module({
	controllers: [CategoryController],
	imports: [MulterModule.register({})],
	providers: [CategoryService, PrismaService],
	exports: [CategoryService]
})
export class CategoryModule {}
