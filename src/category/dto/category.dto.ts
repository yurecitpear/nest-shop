import { IsString, IsOptional, IsBoolean } from 'class-validator'

export class CategoryDto {
	@IsString()
	name: string

	@IsOptional()
	@IsBoolean()
	isRecommended: boolean

	@IsOptional()
	@IsString()
	image: string
}
