import { IsOptional, IsString } from 'class-validator'

export class GetAllCategoryDto {
	@IsOptional()
	@IsString()
	isRecommended?: string
}
