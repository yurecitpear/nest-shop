import { Injectable, NotFoundException } from '@nestjs/common'
import { Prisma } from '@prisma/client'
import { PrismaService } from 'src/prisma.service'
import { generateSlug } from 'src/utils/generate-slug'
import { CategoryDto } from './dto/category.dto'
import { returnCategoryObject } from './return-category.object'
import { GetAllCategoryDto } from './dto/get-all.category.dto'

@Injectable()
export class CategoryService {
	constructor(private prisma: PrismaService) {}

	async byId(id: number) {
		const category = await this.prisma.category.findUnique({
			where: {
				id
			},
			select: returnCategoryObject
		})

		if (!category) {
			throw new Error('Category not found')
		}

		return category
	}

	async bySlug(slug: string) {
		const category = await this.prisma.category.findUnique({
			where: {
				slug
			},
			select: returnCategoryObject
		})

		if (!category) {
			throw new NotFoundException('Category not found')
		}

		return category
	}

	async getAll(dto: GetAllCategoryDto = {}) {
		const filters: Prisma.CategoryWhereInput[] = []
	
		if (dto.isRecommended !== undefined) {
			const isRecommendedValue = dto.isRecommended === 'true'
			filters.push({ isRecommended: isRecommendedValue })
		}
	
		return this.prisma.category.findMany({
			where: {
				AND: filters,
			},
			select: returnCategoryObject,
		})
	}

	async create(dto: CategoryDto, imageUrl: string) {
		return this.prisma.category.create({
			data: {
				name: dto.name,
				slug: generateSlug(dto.name),
				isRecommended: Boolean(dto.isRecommended),
				image: imageUrl
			}
		})
	}

	async update(id: number, dto: CategoryDto) {
		return this.prisma.category.update({
			where: {
				id
			},
			data: {
				name: dto.name,
				isRecommended: dto.isRecommended,
				slug: generateSlug(dto.name)
			}
		})
	}

	async delete(id: number) {
		return this.prisma.category.delete({
			where: {
				id
			}
		})
	}
}
