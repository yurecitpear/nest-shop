import {
	Body,
	Controller,
	Delete,
	Get,
	HttpCode,
	Param,
	Post,
	Put,
	UsePipes,
	Query,
	ValidationPipe,
	UseInterceptors,
	UploadedFile,
} from '@nestjs/common'
import { Auth } from 'src/auth/decorators/auth.decorator'
import { CategoryDto } from './dto/category.dto'
import { GetAllCategoryDto } from './dto/get-all.category.dto'
import { CategoryService } from './category.service'
import { FileInterceptor } from '@nestjs/platform-express'
import { diskStorage } from 'multer'
import { extname } from 'path'

@Controller('categories')
export class CategoryController {
	constructor(private readonly categoryService: CategoryService) {}

	@Get()
	async getAll(@Query() queryDto: GetAllCategoryDto) {
		return this.categoryService.getAll(queryDto)
	}

	// @UsePipes(new ValidationPipe())
	// @Get()
	// async getAll(@Query() queryDto: GetAllProductDto) {
	// 	return this.productService.getAll(queryDto)
	// }

	@Get('by-slug/:slug')
	async getBySlug(@Param('slug') slug: string) {
		return this.categoryService.bySlug(slug)
	}

	@Get(':id')
	@Auth()
	async getById(@Param('id') id: string) {
		return this.categoryService.byId(+id)
	}

	@HttpCode(200)
	@Auth('admin')
	@Post()
	@UseInterceptors(FileInterceptor('image', {
    storage: diskStorage({
      destination: './uploads/category',
      filename: (req, file, callback) => {
        const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1e9)
        const ext = extname(file.originalname)
        const filename = `${file.originalname}-${uniqueSuffix}${ext}`
        callback(null, filename)
      },
    }),
  }))
	async create(@UploadedFile() file: Express.Multer.File, @Body() dto: CategoryDto) {
		const imageUrl = file ? `/uploads/category/${file.filename}` : ''

		const category = await this.categoryService.create(dto, imageUrl)
    return category
	}

	@UsePipes(new ValidationPipe())
	@HttpCode(200)
	@Auth('admin')
	@Put(':id')
	async update(@Param('id') id: string, @Body() dto: CategoryDto) {
		return this.categoryService.update(+id, dto)
	}

	@HttpCode(200)
	@Auth('admin')
	@Delete(':id')
	async delete(@Param('id') id: string) {
		return this.categoryService.delete(+id)
	}
}
