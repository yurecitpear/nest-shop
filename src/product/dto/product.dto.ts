import { Prisma } from '@prisma/client'
import { ArrayMinSize, IsNumber, IsOptional, IsString, IsBoolean, IsArray } from 'class-validator'

export class ProductDto implements Prisma.ProductUpdateInput {
	@IsString()
	name: string

	@IsNumber()
	price: number

	@IsOptional()
	@IsString()
	description: string

	@IsOptional()
	@IsBoolean()
	isRecommended: boolean

	@IsOptional()
	image: string

	@IsOptional()
	@IsNumber()
	liter?: number

	@IsString({ each: true })
	@IsOptional()
	// @ArrayMinSize(1)
	images: string[]

	@IsNumber()
	categoryId: number
}
