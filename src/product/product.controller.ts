import {
	Body,
	Controller,
	Delete,
	Get,
	HttpCode,
	Param,
	Post,
	Put,
	Query,
	UsePipes,
	ValidationPipe,
	UseInterceptors,
	UploadedFile,
	UploadedFiles
} from '@nestjs/common'
import { Auth } from 'src/auth/decorators/auth.decorator'
import { GetAllProductDto } from './dto/get-all.product.dto'
import { ProductDto } from './dto/product.dto'
import { ProductService } from './product.service'
import { FileInterceptor, FilesInterceptor } from '@nestjs/platform-express'
import { diskStorage } from 'multer'
import { extname } from 'path'
import * as fs from 'fs'


@Controller('products')
export class ProductController {
	constructor(private readonly productService: ProductService) {}

	@UsePipes(new ValidationPipe())
	@Get()
	async getAll(@Query() queryDto: GetAllProductDto) {
		return this.productService.getAll(queryDto)
	}

	@Get('similar/:id')
	async getSimilar(@Param('id') id: string) {
		return this.productService.getSimilar(+id)
	}

	@Get('by-slug/:slug')
	async getProductBySlug(@Param('slug') slug: string) {
		return this.productService.bySlug(slug)
	}

	@Get('by-category/:categorySlug')
	async getProductsByCategory(@Param('categorySlug') categorySlug: string) {
		return this.productService.byCategory(categorySlug)
	}

	@Post()
  @UseInterceptors(FileInterceptor('image', {
    storage: diskStorage({
      destination: './uploads',
      filename: (req, file, callback) => {
        const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1e9)
        const ext = extname(file.originalname)
        const filename = `${file.originalname}-${uniqueSuffix}${ext}`
        callback(null, filename)
      },
    }),
  }))
  async createProduct(@UploadedFile() file: Express.Multer.File, @Body() dto: ProductDto) {
    const imageUrl = `/uploads/${file.filename}`
    
    const product = await this.productService.create(dto, imageUrl)
    return product
  }


	@Post('/:id/upload-multiple')
	@UseInterceptors(FilesInterceptor('images', 10, {
		storage: diskStorage({
			destination: (req, file, callback) => {
				const id = req.params.id;
				const uploadPath = `./uploads/${id}/`;
				
				// Проверка существования директории и создание, если не существует
				if (!fs.existsSync(uploadPath)) {
					fs.mkdirSync(uploadPath, { recursive: true });
				}
				
				callback(null, uploadPath);
			},
			filename: (req, file, callback) => {
				const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1e9);
				const ext = extname(file.originalname);
				const filename = `${file.originalname}-${uniqueSuffix}${ext}`;
				callback(null, filename);
			},
		}),
	}))

	@Post('/:id/upload')
	@UseInterceptors(FileInterceptor('image', {
		storage: diskStorage({
			destination: (req, file, callback) => {
				const id = req.params.id;
				const uploadPath = `./uploads/${id}/`;
				
				// Проверка существования директории и создание, если не существует
				if (!fs.existsSync(uploadPath)) {
					fs.mkdirSync(uploadPath, { recursive: true });
				}
				
				callback(null, uploadPath);
			},
			filename: (req, file, callback) => {
				const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1e9);
				const ext = extname(file.originalname);
				const filename = `${file.originalname}-${uniqueSuffix}${ext}`;
				callback(null, filename);
			},
		}),
	}))
	async uploadFile(
		@Param('id') id: number,
		@UploadedFile() file: Express.Multer.File
	) {
		const imageUrl = `/uploads/${id}/${file.originalname}`;
		console.log(file);
		const product = await this.productService.updateImage(id, imageUrl);
		return product;
	}

	async uploadFiles(
		@Param('id') id: number,
		@UploadedFiles() files: Array<Express.Multer.File>
	) {
		const imageUrls = files.map(file => `/uploads/${id}/${file.originalname}`);
		console.log(files);
		const product = await this.productService.addImages(+id, imageUrls);
		return product;
	}


	@UsePipes(new ValidationPipe())
	@HttpCode(200)
	@Put(':id')
	@Auth('admin')
	async updateProduct(@Param('id') id: string, @Body() dto: ProductDto) {
		return this.productService.update(+id, dto)
	}

	@HttpCode(200)
	@Delete(':id')
	@Auth('admin')
	async deleteProduct(@Param('id') id: string) {
		return this.productService.delete(+id)
	}

	@Get(':id')
	@Auth('admin')
	async getProduct(@Param('id') id: string) {
		return this.productService.byId(+id)
	}
}
